import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobile_app/components/photo_item.dart';

class PhotoGalleryPage extends StatefulWidget {
  @override
  _PhotoGalleryPageState createState() => _PhotoGalleryPageState();
}

class _PhotoGalleryPageState extends State<PhotoGalleryPage> {
  List<String> photoUrls = [];

  @override
  void initState() {
    super.initState();
    fetchPhotoUrls();
  }

  Future<void> fetchPhotoUrls() async {
    final baseUrl =
        'https://gitlab.com/ipzk211_moo/mobileappfetchdata/raw/main/gallery_photos/';
    List<String> urls = [];


    for (int index = 1;; index++) {
      final response = await http.head(Uri.parse('$baseUrl$index.jpg'));
      if (response.statusCode == 200) {
        urls.add('$baseUrl$index.jpg');
      } else {
        break;
      }
    }

    setState(() {
      photoUrls = urls;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Фотогалерея'),
        centerTitle: true,
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
        ),
        itemCount: photoUrls.length,
        itemBuilder: (BuildContext context, int index) {
          return PhotoItem(photoUrls[index]);
        },
      ),
    );
  }
}
