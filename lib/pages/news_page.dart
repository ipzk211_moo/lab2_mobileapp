import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:mobile_app/components/news_item.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  List<dynamic> newsData = [];

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  Future<void> fetchData() async {
    final response = await http.get(Uri.parse('https://gitlab.com/ipzk211_moo/mobileappfetchdata/-/raw/main/news.json'));

    if (response.statusCode == 200) {
      setState(() {
        newsData = jsonDecode(response.body);
      });
    } else {
      throw Exception('Не вдалося завантажити дані');
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Новини'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: newsData.length,
        itemBuilder: (context, index) {
          final newsItem = newsData[index];
          return NewsItem(
            imageUrl: "https://gitlab.com/ipzk211_moo/mobileappfetchdata/raw/main/news_photos/"+newsItem['id']+".jpg",
            title: newsItem['title'],
            date: newsItem['date'],
            description: newsItem['content_small'],
          );
        },
      ),
    );
  }
}
