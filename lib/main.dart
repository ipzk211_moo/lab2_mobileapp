import 'package:flutter/material.dart';
import 'package:mobile_app/pages/news_page.dart';
import 'package:mobile_app/pages/photo_gallery_page.dart';
import 'package:mobile_app/pages/registration_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  List<Widget> _widgetOptions = <Widget>[
    NewsPage(),
    PhotoGalleryPage(),
    RegistrationPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 8.0),
              child: Image(
                width: 100,
                image: AssetImage('assets/polytechnik_logo.png'),

              ),
            ),
            Spacer(),
            Text(
              'FlutterMobileApp',
              style: TextStyle(fontSize: 18),
            ),
          ],
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(56.0),
          child: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.article),
                label: 'Новини',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.photo),
                label: 'Фотогалерея',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Реєстрація',
              ),
            ],
            currentIndex: _selectedIndex,
            selectedItemColor: Colors.blue,
            onTap: _onItemTapped,
          ),
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomSheet: Container(
        color: Colors.grey[200],
        padding: EdgeInsets.symmetric(horizontal: 70.0),

        child: Text('Виконала Мачушник Олена, ІПЗк-21-1'),
      ),
    );
  }

}
